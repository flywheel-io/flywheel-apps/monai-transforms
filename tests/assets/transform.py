"""A simple transform"""
from monai.transforms import CenterSpatialCropd, Compose, LoadImaged, SaveImaged

transform = Compose(
    [
        LoadImaged(["img"]),
        CenterSpatialCropd(["img"], [64, 64, 1]),
        # SaveImaged(["img"], output_postfix="t", output_ext=".nii.gz"),
        SaveImaged(["img"]),
    ]
)
