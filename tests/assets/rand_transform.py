"""A simple transform that randomly crops and saves as NIfTI."""
from monai.transforms import Compose, EnsureType, LoadImage, RandSpatialCrop, SaveImage

transform = Compose(
    [
        LoadImage(image_only=True),
        RandSpatialCrop((128, 128, 128), random_size=False),
        EnsureType(),
        SaveImage(
            output_postfix="test",
            output_ext=".nii",
            resample=False,
            print_log=False,
        ),
    ]
)
