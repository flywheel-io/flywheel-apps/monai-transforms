"""Module to test parser.py"""
from unittest.mock import MagicMock

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_monai_transforms.parser import parse_config


def test_parse_config(tmp_path):
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.get_input_path.return_value = tmp_path
    gear_context.config = {"number-of-iterations": 1}
    input_file, transform_file, iter_num = parse_config(gear_context)

    assert input_file == tmp_path
    assert transform_file == tmp_path
    assert iter_num == 1
