"""Module to test transform.py"""
import os
import shutil
import tempfile
from pathlib import Path

import pytest
from monai.transforms import (
    CenterSpatialCropd,
    Compose,
    LoadImage,
    LoadImaged,
    Randomizable,
    RandSpatialCrop,
    SaveImage,
    SaveImaged,
)
from nibabel.testing import data_path
from pytest_mock import mocker

from fw_gear_monai_transforms.transform import (  # update_transforms_postfix,
    apply_transform,
    check_rand_transformation,
    get_transform,
    update_transforms_saver,
    validate_transform,
)

ASSETS_DIR = Path(__file__).parent / "assets"


def test_update_transforms_saver():
    transform = Compose([LoadImaged(["img"]), SaveImaged(["img"])])
    transform = update_transforms_saver(transform, output_dir="/tmp")
    assert transform.transforms[-1]._saver.saver.output_dir == "/tmp"


@pytest.mark.parametrize(
    "transform,is_valid",
    [
        (Compose(), False),
        (Compose([SaveImaged]), False),
        (Compose([LoadImaged(["img"])]), False),
        (Compose([LoadImaged(["img"]), SaveImaged(["img"])]), True),
    ],
)
def test_validate_transform(transform, is_valid):
    errs = validate_transform(transform)
    assert (len(errs) == 0) is is_valid


def test_apply_transform(tmpdir):
    input_mod_path = ASSETS_DIR / "rand_transform.py"
    example_file = os.path.join(data_path, "example4d.nii.gz")
    apply_transform(example_file, input_mod_path, output_dir=tmpdir, iter_num=2)
    assert len(os.listdir(tmpdir)) == 2
    filename = os.listdir(tmpdir)[0]
    assert filename == "0_example4d.nii"
    filename = os.listdir(tmpdir)[1]
    assert filename == "1_example4d.nii"

    input_mod_path = ASSETS_DIR / "transform.py"
    apply_transform(example_file, input_mod_path, output_dir=tmpdir, iter_num=1)
    assert len(os.listdir(tmpdir)) == 3


@pytest.fixture()
def my_transform():
    return Compose(
        [
            LoadImaged(["img"]),
            CenterSpatialCropd(["img"], [64, 64, 1]),
            SaveImaged(["img"]),
        ]
    )


@pytest.fixture()
def my_rand_transform():
    return Compose(
        [
            LoadImaged(["img"]),
            RandSpatialCrop(random_size=False, roi_size=2),
            SaveImaged(["img"]),
        ]
    )


def test_get_transform(my_transform, my_rand_transform):
    tr = get_transform(my_transform, 0)
    assert isinstance(tr, LoadImaged)
    tr = get_transform(my_rand_transform, 1)
    assert isinstance(tr, Randomizable)


# def test_update_transforms_postfix(my_rand_transform):
#     tr = update_transforms_postfix(my_rand_transform, 2)
#     assert tr.transforms[-1]._saver.saver.output_postfix == "2"


def test_check_rand_transformation(my_transform, my_rand_transform):
    assert check_rand_transformation(my_transform) is False
    assert check_rand_transformation(my_rand_transform) is True
