# Release notes

## 0.1.2

__Enhancements__:

* Add support for Stochastic transform;
* Randomizable transformations will be run multiple times saving an image at each iteration.

## 0.1.1

__Enhancements__:

* Add support for Compose transforms that starts with LoadImage (instead of LoadImaged)

## 0.1.0

Initial release
