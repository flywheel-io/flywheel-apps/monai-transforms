#!/usr/bin/env python
"""The run script."""
import logging

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_monai_transforms.parser import parse_config
from fw_gear_monai_transforms.transform import apply_transform

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run."""
    input_file, input_mod, iter_num = parse_config(context)
    apply_transform(
        input_file, input_mod, output_dir=context.output_dir, iter_num=iter_num
    )


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as context:
        context.init_logging()
        main(context)
