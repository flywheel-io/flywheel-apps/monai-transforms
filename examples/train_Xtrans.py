"""A simple transform that randomly crops and saves as NIfTI."""
from monai.transforms import (
    AddChannel,
    Compose,
    EnsureType,
    LoadImage,
    NormalizeIntensity,
    RandRotate90,
    RandSpatialCrop,
    SaveImage,
)

transform = Compose(
    [
        LoadImage(image_only=True),
        NormalizeIntensity(),
        AddChannel(),
        RandSpatialCrop((128, 128, 128), random_size=False),
        RandRotate90(prob=0.5, spatial_axes=(0, 1)),
        EnsureType(),
        SaveImage(
            output_postfix="Xtrans",
            output_ext=".nii",
            resample=False,
            separate_folder=False,
            print_log=False,
        ),
    ]
)
