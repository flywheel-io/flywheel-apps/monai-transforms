"""A simple transform that crops and saves as PNG."""
from monai.transforms import (
    AddChanneld,
    CenterSpatialCropd,
    Compose,
    LoadImaged,
    SaveImaged,
)

transform = Compose(
    [
        LoadImaged(["img"]),
        AddChanneld(["img"]),
        CenterSpatialCropd(["img"], [64, 64, 1]),
        # CastToTyped(["img"], dtype=np.int)
        SaveImaged(["img"], output_postfix="t", output_ext=".png", scale=65535),
    ]
)
