"""A simple transform that Normalize the intensity of an image and saves as NiFTI."""
from monai.transforms import (
    AddChanneld,
    Compose,
    LoadImaged,
    SaveImaged,
    ScaleIntensityd,
)

transform = Compose(
    [
        LoadImaged(["img"]),
        AddChanneld(["img"]),
        ScaleIntensityd(["img"]),
        SaveImaged(["img"], output_postfix="t", output_ext=".nii", scale=65535),
    ]
)
