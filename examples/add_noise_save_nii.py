"""A simple transform that perform a add some Gaussian noise."""
from monai.transforms import (
    AddChanneld,
    Compose,
    LoadImaged,
    RandGaussianNoised,
    SaveImaged,
)

transform = Compose(
    [
        LoadImaged(["img"]),
        AddChanneld(["img"]),
        RandGaussianNoised(["img"]),
        SaveImaged(["img"], output_postfix="t", output_ext=".nii.gz"),
    ]
)
